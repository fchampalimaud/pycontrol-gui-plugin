# !/usr/bin/python3
# -*- coding: utf-8 -*-

import logging

from pysettings import conf

if conf.PYFORMS_USE_QT5:
    from PyQt5.QtWidgets import QMessageBox
else:
    from PyQt4.QtGui import QMessageBox

import pyforms as app
from pyforms import BaseWidget
from pyforms.Controls import ControlText
from pyforms.Controls import ControlButton
from pyforms.Controls import ControlCombo

from pycontrolapi.models.task import Task
from pycontrolapi.models.experiment import Experiment
from pycontrolapi.models.project import Project
from pycontrolapi.exceptions.run_setup import RunSetupError

logger = logging.getLogger(__name__)


class ExperimentWindow(Experiment, BaseWidget):
    """
    Define here which fields from the board model should appear on the details section.

    The model fields shall be defined as UI components like text fields, buttons, combo boxes, etc.

    You may also assign actions to these components.

    **Private attributes**

    _name
        Field to edit experiment name

        :type: :class:`pyforms.Controls.ControlText`

    _button_run_all
        Button to run task for all subjects. Pressing the button fires the event :class:`ExperimentWindow._ExperimentWindow__button_run_all_evt`.

        :type: :class:`pyforms.Controls.ControlButton`

    _button_upload_all
        Button to upload task for all subjects. Pressing the button fires the event :class:`ExperimentWindow._ExperimentWindow__button_upload_all_evt`.

        :type: :class:`pyforms.Controls.ControlButton`

    _button_restore
        Button to restore task variables. Pressing the button fires the event :class:`ExperimentWindow._ExperimentWindow__button_restore_evt`.

        :type: :class:`pyforms.Controls.ControlButton`

    _task
        Combo box of available tasks. Current selected task is the task associated for this experiment
        and all its subjects. Selecting a different task fires the event :class:`ExperimentWindow._ExperimentWindow__task_changed_evt`.

        :type: :class:`pyforms.Controls.ControlCombo`

    **Methods**

    """

    def __init__(self, project):
        # type: (Project) -> None
        """

        :param project: project where this experiment belongs
        :type project: pycontrolgui.models.project.Project
        """
        BaseWidget.__init__(self, 'Experiment')
        self.layout().setContentsMargins(5, 10, 5, 5)

        self._name = ControlText('Exp. name')
        self._button_restore = ControlButton('Restore values', helptext='Restores variables values from last session')
        self._button_run_all = ControlButton('Run all')
        self._button_upload_all = ControlButton('Upload all')
        self._task = ControlCombo('Task')

        self._formset = [
            '_name',
            '_task',
            ('_button_upload_all', '_button_run_all'),
            'info:Restore persistent variables from previous sessions',
            '_button_restore',
            ' '
        ]

        Experiment.__init__(self, project)

        self.reload_tasks()

        self._name.changed_event = self.__name_changed_evt
        self._task.changed_event = self.__task_changed_evt

        self._button_restore.value = self.__button_restore_evt
        self._button_run_all.value = self.__button_run_all_evt
        self._button_upload_all.value = self.__button_upload_all_evt

    def __button_upload_all_evt(self):
        """
        Defines behavior of the button 'Upload all'.

        This methods is called every time the user presses the button.
        """

        if not self.setups:
            logger.warning("Cannot upload task because there are no subjects.")
            QMessageBox.warning(self,
                                      "Warning",
                                      "To upload task, please add subject first.")

        else:
            for setup in self.setups:
                try:
                    setup.upload_task()
                except RunSetupError as err:
                    QMessageBox.warning(self, "Warning", str(err))
                except Exception as err:
                    QMessageBox.critical(self, "Unexpected Error", str(err))

    def __button_run_all_evt(self):
        """
        Defines behavior for the button 'Run all'.

        This methods is called every time the user presses the button.
        """

        if not self.setups:
            logger.warning("Cannot run experiment because there are no subjects.")
            QMessageBox.warning(self,
                                      "Warning",
                                      "To run experiment, please add subject first.")

        else:
            for setup in self.setups:
                try:
                    setup.run_task()
                except RunSetupError as err:
                    QMessageBox.warning(self, "Warning", str(err))
                except Exception as err:
                    QMessageBox.critical(self, "Unexpected Error", str(err))

    def __button_restore_evt(self):
        """
        Defines behavior for the button 'Restore values'.

        For each subject, the variables' values are restored from the most recent session file.

        This methods is called every time the user presses the button.
        """

        if not self.setups:
            QMessageBox.warning(self,
                                      "No subjects found",
                                      "Experiment has no subjects. No task variables restored.")

        else:
            for setup in self.setups:
                try:
                    setup.restore_task_variables_from_session()
                except RunSetupError as err:
                    QMessageBox.warning(self, "Warning", str(err))
                except Exception as err:
                    QMessageBox.critical(self, "Unexpected Error", str(err))

    def __task_changed_evt(self):
        """

        This methods is called every time the user presses the button.
        """
        self.task = self._task.value
        self.update_ui()

    def __name_changed_evt(self):
        if not hasattr(self, '_update_name') or not self._update_name:
            self.name = self._name.value

    def reload_tasks(self, current_selected_task=None):
        # type: (Task) -> None
        """
        Reload tasks now

        :param current_selected_task: current selected task
        :type current_selected_task: pycontrolgui.models.task.Task
        """
        self._task.clear()
        self._task.add_item('', 0)
        for task in self.project.tasks:
            self._task.add_item(task.name, task)
        self._task.current_index = 0
        if current_selected_task:
            self.task = current_selected_task

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, value):
        self._update_name = True  # Flag to avoid recurse calls when editing the name text field
        self._name.value = value
        self._update_name = False

    @property
    def task(self):
        """
        Property that holds the task currently associated with this experiment.

        This property returns the current value stored in the combo box of tasks (which should be a task)

        :type task: Task
        """
        if isinstance(self._task.value, str): return None
        return self._task.value

    @task.setter
    def task(self, value):
        last_task = self._task.value
        if isinstance(value, str): value = self.project.find_task(value)
        self._update_name = True  # Flag to avoid recurse calls when editing the name text field
        self._task.value = value

        try:
            for setup in self.setups:
                setup.task = value
                setup.board_task.load_task_details()
        except FileNotFoundError as err:
            logger.warning(str(err))

            QMessageBox.about(self,
                                    "Task file does not exists yet.",
                                    "The task file does not exists yet.\nPlease save the project to create the task file.")

            self._task.value = last_task

        self._update_name = False


if __name__ == "__main__":
    # Execute the application
    app.start_app(ExperimentWindow)
