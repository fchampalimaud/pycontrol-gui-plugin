# DO NOT CHANGE THIS FILE
# DUPLICATE AND SAVE IT AS uninstall-requirements-dev-YOURNAME.txt
# run like this: pip3 uninstall -r uninstall-requirements-dev-YOURNAME.txt -y

# services libraries
#logging-bootstrap
#pyserial>=3.1.1
#Send2Trash>=1.3.0

# pyforms libraries
pysettings
pyforms
pyforms-generic-editor
pybranch

# pycontrol libraries
pyboard-communication
pycontrol-api

# plugins
pycontrol-gui-plugin-experiment-macros
pycontrol-gui-plugin-export-code
pycontrol-gui-plugin-remote-project
pycontrol-gui-plugin-session-broadcast
session-log-plugin
pycontrol-gui-plugin-terminal
pycontrol-gui-plugin-timeline


