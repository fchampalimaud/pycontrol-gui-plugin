# PyControlGUI

**PyControlGUI** is a *Graphical User Interface (GUI)* for the [PyControl framework](https://bitbucket.org/takam/pycontrol/wiki/Home). It is written in Python3 and built on top of [PyForms](https://github.com/UmSenhorQualquer/pyforms) and [PyControlAPI](https://bitbucket.org/fchampalimaud/pycontrol-api) libraries. 

Please see [Wiki](https://pycontrol.readthedocs.io/en/latest/) for more information.

![pyControlGUI frontpage](frontpage.png)