.. pyboard_communication documentation master file, created by
   sphinx-quickstart on Thu Dec 15 12:00:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pycontrol-gui's documentation!
=========================================
This project implements a GUI version of the pyControl model, as described on the pycontrol-api library.

For pyControl generic information please refer to `<http://pycontrol.readthedocs.io>`_.


Contents:

.. toctree::
   :maxdepth: 2

   getting_started
   user_interface
   model_reference/index
   plugins/index
   about/index





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
