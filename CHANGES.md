## v 1.5.1
Fixes import task bugs
Fixes project name changed bug
Fixes projects window incompatibility with other plugins
Fixes bug which would prevent dockwindow from show up when creating new project
Fixes error deleting task closes app (PyForms)

## v 1.5.0 (2017/04/20)
* Housekeeping, removes old unneeded files
* Adapt for pyforms-generic-editor latest version
* Updates requirements, gitignore, etc
* Minor code enhancements
* Adds support for qt5
* Catch exception if trying to load session contents but session file doesn’t exist yet
* Do not quit app on faulty session file
* Better error handling for invalid user settings file, we now show a recover window where user can edit settings (pyforms-generic-editor)

## v.1.4.0 (2017/02/17)
* Compliance with pyforms v1.0.beta
* Updates dependencies versions

## v.1.3.1 (2016/12/02)
Implemented changes according to: https://bitbucket.org/fchampalimaud/pycontrol-gui/wiki/Proposed%20GUI%20enhancements
* Modify data file format (what is missing is include the pyboards hardware unique ID and save the name of the hardware definition)
* On the GUI Setup was replaced by Subject (the folder structure was not changed yet because it requires more changes and it is more error-prone)

## v.1.3.0 (1.3) (2016/11/22)
Merge with current framework.
